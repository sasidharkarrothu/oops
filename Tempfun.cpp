#include<iostream>
using namespace std;
class Template{
    public:
    int result = 0;
    template<typename myDataType1>
    void add(myDataType1 a, myDataType1 b){
        myDataType1 result;
        result = a+b;
        cout<<result<<endl;
    }
};
int main(){
    Template obj;
    obj.add<int>(2,3);
    obj.add<float>(2.3,3.3);
}
