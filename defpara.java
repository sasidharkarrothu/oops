import java.lang.*;
import java.util.*;
class Student{
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode;
    
    Student(String fname,int roll,double spercent,String cname,int ccode) {
        fullName = fname;
        rollNum = roll;
        semPercentage = spercent;
        collegeName = cname;
        collegeCode = ccode;
    }
    
    void displayData()
    {   
        System.out.println("\nSTUDENT DETAILS");
        System.out.println(" Full Name : " + fullName);
        System.out.println(" Roll No. : " + rollNum);
        System.out.println(" Sem Percentage : " + semPercentage);
        System.out.println(" College Name : " + collegeName);
        System.out.println(" College Code : " + collegeCode);
    }

    protected void finalize()
    {
        System.out.println("Object destroyed by Garbage Collector");
    }
        
    public static void main(String[] args) 
    { 
        Student s1 = new Student("Manivi",384,93,"MVGR",33);
        Student s2 = new Student("Krishi",582,87,"MVGR",33);
        s1.displayData();
        s2.displayData();
        s1.finalize();
        s2.finalize();
    }
}