class mthdovl {
    public void print(int x) {
        System.out.println("Printing integer: " + x);
    }

    public void print(float x) {
        System.out.println("Printing float: " + x);
    }
}

class Child extends mthdovl {
    public void print(String x) {
        System.out.println("Printing string: " + x);
    }

    public static void main(String args[]) {
        Child obj = new Child();
        obj.print(10);
        obj.print(10.0f);
        obj.print("Hello World");
    }
}
