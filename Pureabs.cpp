#include<iostream>
using namespace std;
class A{
public:
int a,b;
void add(){
    cin>>a>>b;
}
virtual int addition()=0;
};
class B : public A {
    public:
    int addition() {
        return a+b;
    }
};
class C : public A{
    public:
    int addition() {
        return 10+a+b;
    }
};
int main(){
    B obj;
    C obj1;
    cout<<"Enter the a and b values:"<<endl;
    obj.add(); 
    cout<<"The sum of a and b is:"<<obj.addition()<<endl;
    obj1.add();
    cout<<"The sum of a and b with third unknown variable is:"<<obj1.addition()<<endl;
}
