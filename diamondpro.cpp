#include<iostream>
using namespace std;
class A
{
public:
    void display()
        {
            cout<<"APPLE"<<endl;
        }
};
class B
{
     public:
    void show()
        {
            cout<<"BOY"<<endl;
        }
};
class C: public A, public B
{
 
};
int main()
{
    C obj;
    obj.display();
    obj.show();
    return 0;
}
