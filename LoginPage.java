import java.awt.*;
import java.awt.event.*;


public class LoginPage extends Frame implements ActionListener {
  private Label usernameLabel;
  private Label passwordLabel;
  private TextField usernameField;
  private TextField passwordField;
  private Button loginButton;


  public LoginPage() {
    setLayout(new GridLayout(3, 2, 10, 10));


    usernameLabel = new Label("Username:");
    add(usernameLabel);


    usernameField = new TextField(20);
    add(usernameField);


    passwordLabel = new Label("Password:");
    add(passwordLabel);


    passwordField = new TextField(20);
    passwordField.setEchoChar('*');
    add(passwordField);


    loginButton = new Button("Login");
    add(loginButton);
    loginButton.addActionListener(this);


    setTitle("Login Page");
    setSize(300, 150);
    setVisible(true);
  }


  public void actionPerformed(ActionEvent e) {
    String username = usernameField.getText();
    String password = passwordField.getText();


    if (username.equals("admin") && password.equals("admin")) {
      System.out.println("Login successful");
    } else {
      System.out.println("Invalid username or password");
    }
  }


  public static void main(String[] args) {
    LoginPage page = new LoginPage();
  }
}


