#include<stdio.h>
struct Employee {
    char name[35];
    char department[10];
    char designation[20];
    int emp_id;
    float salary;
} emp[10];

int main() {
    int n;
    FILE *f;
    f = fopen("Emp_details.txt","w");

    printf("Enter the no.of employees details ");
    scanf("%d",&n);
    for(int i=1;i<=n;i++) {
    printf("Enter the Employee %d name,id,department,designation,salary : ",i);
    scanf("%s%d%s%s%f",emp[i].name,&emp[i].emp_id,emp[i].department,emp[i].designation,&emp[i].salary);
    }
    for(int j=1;j<=n;j++) {
    fprintf(f,"Employee %d details :\n",j);
    fprintf(f," Employee name : %s\n Employee id : %d\n Employee department : %s\n Employee designation : %s\n Employee salary : %f\n",emp[j].name,emp[j].emp_id,emp[j].department,emp[j].designation,emp[j].salary);
    }
    fclose(f);
    char c;
    FILE *fp;
    fp=fopen("Emp_details.txt","r");
    if(fp==NULL)
        printf("Content doesn't exist");
    do{
        c = fgetc(fp);
        printf("%c",c);
    } while (c!= EOF);
    fclose(fp);
}
