public class Parent {
    void father() {
        System.out.println("YOU SHOULD EAT WELL!!!");
    }


    void mother() {
        System.out.println("YOU SHOULD GO AND PLAY!!!");
    }
}


class Child extends Parent {
    void girl() {
        System.out.println("I WILL EAT AND PLAY WELL.");
    }


    public static void main(String[] args) {
        Child obj = new Child();
        obj.father();
        obj.mother();
        obj.girl();
    }


}
