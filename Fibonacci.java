package krishi;


public class Fibonacci {
    public void fibseries(int n) {
        int a = 0, b = 0, c = 1, i;
        System.out.print("The " + n + "th Fibonacci Series is : ");
        System.out.print(b + " " + c);
        for (i = 2; i < n; ++i) {
            a = b + c;
            System.out.print(" " + a);
            b = c;
            c = a;
        }
        System.out.print("\n");
    }


    public void fibnum(int n) {
        int a = 0, b = 0, c = 1, i;
        if (n == 1) {
            System.out.println("The " + n + " number of Fibonacci is : 0");
        } else if (n == 2) {
            System.out.println("The " + n + " number of Fibonacci is : 1");
        } else {
            for (i = 2; i < n; ++i) {
                a = b + c;
                b = c;
                c = a;
            }
            System.out.println("The " + n + " number of Fibonacci is : " + a);
        }
    }
}








import krishi.*;


public class Fib {
    public static void main(String[] args) {
        Fibonacci obj = new Fibonacci();
        obj.fibnum(10);
        obj.fibseries(5);
    }
}
