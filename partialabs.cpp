#include <iostream>
using namespace std;
class Shape {
public:
    virtual void draw() = 0;
};


class Rectangle : public Shape {
public:
    void draw() {
        cout << "Drawing a rectangle." << endl;
    }
};


class Circle : public Shape {
public:
    void draw() {
        cout << "Drawing a circle." << endl;
    }
};
int main() {
    Rectangle obj;
    Circle obj1;
    obj.draw();
    obj1.draw();
    return 0;
}
