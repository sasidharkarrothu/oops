#include <iostream>
using namespace std;
class A{
private:
    int real, img;

public:
    A(int r = 0, int i = 0)
    {
        real = r;
        img = i;
    }

    void print()
    {
        cout << real << " + i" << img << endl;
    }

A operator*(const A& new_op)
{
    A obj;
    obj.real = real * new_op.real;
    obj.img = img * new_op.img;
    return obj;
}
};

int main()
{
    A op1(2, 4), op2(4, 8);
    A op3= op1 * op2;
    op3.print();
    return 0;
}
