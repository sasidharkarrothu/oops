abstract class Shape {
    public abstract void draw();


    public void setColor(String color) {
        System.out.println("Setting color to " + color);
    }
}


class Circle extends Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a circle");
    }
}


class Rectangle extends Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a rectangle");
    }
}


public class Partabst {
    public static void main(String args[]) {
        Shape circle = new Circle();
        Shape rectangle = new Rectangle();


        circle.draw();
        rectangle.draw();


        circle.setColor("Red");
        rectangle.setColor("Blue");
    }
}
