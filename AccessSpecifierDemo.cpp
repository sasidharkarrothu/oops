public class AccessSpecifierDemo {
    private int priVar;
    protected int proVar;
    public int pubVar;

    void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    void getVar() {
        System.out.println("The number " + priVar + " is private");
        System.out.println("The number " + proVar + " is protected");
        System.out.println("The number " + pubVar + " is public");
    }

    public static void main(String args[]) {
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(10, 20, 30);
        obj.getVar();
    }
}
