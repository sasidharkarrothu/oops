#include<iostream>
using namespace std;
class A{
    public:
    void father() {
        cout<<"VERY GOOD"<<endl;
    }
};
class B : public A{                                       //single
    public:
    void boy() {
        cout<<"I AM VERY GOOD." <<endl;
    }
};
class G : public B{                                       //multilevel
    public:
    void mother(){
        cout<<"I WILL COOK."<<endl;
    }
};
class C : public A, public B{
     public:
    void girl() {
        cout<<"I WILL STUDY AND GET GOOD MARKS." <<endl;  //multiple
    }
};
class D : public A{                               //hierarchical
    public:
    void display(){
        cout<<"HIRARCHIECAL"<<endl;
    }
};
class E : public A{
    public:
    void display1(){
        cout<<"Hirarchiecal"<<endl;
    }
};
class F : public A, public B, public C{                     //hybrid
    void display2(){
        cout<<"Hybrid"<<endl;
    }
};


int main() {
   B obj3;
   G obj5;
   C obj1;
   D obj2;
   F obj4;
   cout<<"SINGLE"<<endl;
   obj3.father();
   obj3.boy();
   cout<<"MULTILEVEL"<<endl;
   obj5.mother();
   obj5.boy();
   cout<<"MULTIPLE"<<endl;
   obj2.father();
   obj1.boy();
   obj1.girl();
   cout<<"HIRARCHIECAL"<<endl;
   obj2.father();
   obj2.display();
   cout<<"HYBRID"<<endl;
   obj2.father();
   obj3.boy();
   obj4.girl();
}
