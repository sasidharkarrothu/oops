class Methodovr {
    public void makeSound() {
        System.out.println("Animal is making a sound");
    }
}

class Dog extends Methodovr {
    @Override
    public void makeSound() {
        System.out.println("Woof");
    }

    public static void main(String args[]) {
        Methodovr animal = new Methodovr();
        Dog dog = new Dog();

        animal.makeSound();
        dog.makeSound();
    }
}
