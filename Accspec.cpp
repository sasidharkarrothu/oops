#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;

    void setVar(int priValue, int proValue, int pubValue){
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    void getVar(){
        cout<<"The number "<< priVar<<" is private"<<endl;
        cout<<"The number "<< proVar<<" is protected"<<endl;
        cout<<"The number "<< pubVar<<" is public"<<endl;
    }
};

int main(){
    AccessSpecifierDemo obj;
    obj.setVar(10,20,30);
    obj.getVar();
}
